<?php

namespace Sinta\Generators\Laravel;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Sinta\Generators\FileGenerator;

abstract class GeneratorCommand extends Command
{
    protected $type;

    protected $stub_directory;


    public function __construct()
    {
        parent::__construct();
        $this->stub_directory = __DIR__.'/stubs';
    }


    public function getStubDirectory()
    {
        return $this->stub_directory;
    }

    public function setStubDirectory($path)
    {
        $this->stub_directory = $path;
    }

    public function handle()
    {
        $generator = FileGenerator::make($this->getRootDirectory(), $this->getStubDirectory());
        if ($this->generate($generator) === false) {
            return false;
        }
        $this->info($this->type.' created successfully.');
    }

    abstract protected function generate(FileGenerator $generator);


    protected function convertToFullQualifyClassName($name)
    {
        if (Str::contains($name, '/')) {
            $name = str_replace('/', '\\', $name);
        }
        if (Str::startsWith($name, '\\')) {
            $name = substr($name, 1);
        }
        else {
            $name = $this->getDefaultNamespace().'\\'.$name;
        }
        return $this->filterFullQualifyClassName($name, function ($part) {
            return ucfirst($part);
        });
    }


    protected function filterFullQualifyClassName($fqcn, callable $callback)
    {
        $parts = explode('\\', $fqcn);
        foreach ($parts as &$part) {
            $part = call_user_func($callback, $part);
        }
        return implode('\\', $parts);
    }


    protected function splitFullQualifyClassName($fqcn)
    {
        $parts = explode('\\', $fqcn);
        $classname = array_pop($parts);
        return [implode('\\', $parts), $classname];
    }



    protected function getRootNamespace()
    {
        return trim($this->laravel->getNamespace(), '\\');
    }

    protected function getDefaultNamespace()
    {
        return $this->getRootNamespace();
    }

    protected function getRootDirectory()
    {
        return $this->laravel['path'];
    }

    protected function getRelativePath($fqcn)
    {
        if (strpos($fqcn, $this->getRootNamespace().'\\') === 0) {
            $relative = substr($fqcn, strlen($this->getRootNamespace()) + 1);
        }
        else {
            $relative = $fqcn;
        }
        return str_replace('\\', '/', $relative);
    }

}