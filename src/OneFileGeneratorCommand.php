<?php

namespace Sinta\Generators\Laravel;

abstract class OneFileGeneratorCommand extends GeneratorCommand
{
    protected function generate(FileGenerator $generator)
    {
        $fqcn = $this->convertToFullQualifyClassName($this->getNameInput());
        $path = $this->getRelativePath($fqcn).'.php';
        if ($generator->exists($path)) {
            $this->error($this->type.' already exists!');
            return false;
        }
        return $this->generateFile($generator, $path, $fqcn);
    }

    protected function getNameInput()
    {
        return $this->argument('name');
    }

    abstract protected function generateFile(FileGenerator $generator, $path, $fqcn);
}